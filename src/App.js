import React, { Component } from "react";
import axios from "axios";
import PostList from "./components/PostList";
import Navbar from "./components/Navbar";
import Title from "./components/Title";
import styled from "styled-components";

class App extends Component {
  state = { posts: [] };

  componentDidMount() {
    var self = this;
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then(function(resp) {
        self.setState({ posts: resp.data });
      });
  }
  render() {
    return (
      <Container>
        <Navbar>
          <Title> Compost </Title>
        </Navbar>
        <Body>
          <PostList posts={this.state.posts} />
        </Body>
      </Container>
    );
  }
}

const Container = styled.div`
  background: #e2e1e0;
`;

const Body = styled.div`
  width: 600px;
  margin: 0 auto;
`;

export default App;
