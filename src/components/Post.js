import React from "react";
import axios from "axios";
import Modal from "react-responsive-modal";
import styled from "styled-components";
import Comments from "./Comments";

class Post extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			open: false
		};

		this.onOpenModal = this.onOpenModal.bind(this);
		this.onCloseModal = this.onCloseModal.bind(this);
	}

	onOpenModal = () => {
		this.setState({ open: true });
	};

	onCloseModal = () => {
		this.setState({ open: false });
	};

	render() {
		const open = this.state.open;
		return (
			<Card>
				<TextContainer>
					<b>{this.props.post.title}</b>
				</TextContainer>
				<Button onClick={this.onOpenModal}> Show </Button>
				<Modal open={open} onClose={this.onCloseModal} center>
					<h2>{this.props.post.title}</h2>
					<p>{this.props.post.body}</p>
					<hr />
					<Comments postId={this.props.post.id} />
				</Modal>
			</Card>
		);
	}
}

const Card = styled.div`
	background: #fff;
	border-radius: 2px;
	display: inline-block;
	margin: 10px;
	padding: 20px;
	position: relative;
	width: 100%;
	box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
	transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);

	&:hover {
		box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
	}
`;

const TextContainer = styled.div`
	width: 70%;
	padding: 2px 16px;
	float: left;
`;

const Button = styled.button`
	float: right;
	background-color: #4caf50;
	border: none;
	color: white;
	// padding: 15px 30px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;
	border-radius: 5px;
`;

export default Post;
