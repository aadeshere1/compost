import React from "react";
import styled from "styled-components";
import Post from "./Post";

class PostList extends React.Component {
	render() {
		const { posts } = this.props;
		return (
			<div>
				{posts.map(function(post) {
					return (
						<div>
							<Post post={post} />
							{console.log(post)}
						</div>
					);
				})}
			</div>
		);
	}
}

export default PostList;
