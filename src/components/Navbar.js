import React from "react";
import styled from "styled-components";

const Navbar = styled.div`
	width: 100%;
	background-color: #757575;
	text-align: center;
	overflow: hidden;
`;

export default Navbar;
