import React from "react";
import axios from "axios";
import styled from "styled-components";

class Comments extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			comments: []
		};
	}

	componentDidMount() {
		var self = this;
		axios
			.get(
				`https://jsonplaceholder.typicode.com/comments?postId=${
					this.props.postId
				}`
			)
			.then(function(resp) {
				self.setState({ comments: resp.data });
			});
	}

	render() {
		return (
			<div>
				<h3>Comments</h3>
				{this.state.comments.map(function(comment) {
					return (
						<div>
							<h5>{comment.name}</h5>
							<h6>{comment.email}</h6>
							<p>{comment.body}</p>
						</div>
					);
				})}
			</div>
		);
	}
}

export default Comments;
